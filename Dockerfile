FROM node:9.9

ENV APP_DIR=/usr/src/app

RUN mkdir -p $APP_DIR
WORKDIR $APP_DIR

COPY . $APP_DIR

RUN npm install
RUN npm run build
RUN npm prune --production

CMD ["npm","run", "server"]
