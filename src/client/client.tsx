import { ClientRouter } from '@src/client/ClientRouter';
import React from 'react';
import { hydrate, render } from 'react-dom';
import '../styles/index.scss';

const AppWithStore = <ClientRouter/>;

if (process.env.NODE_ENV === 'development') {
  const root = document.createElement('div');
  root.id = 'root';
  document.body.appendChild(root);

  render(AppWithStore, root);
} else {
  const root = document.getElementById('root');
  hydrate(AppWithStore, root);
}
