import { MainLayout } from '@src/layouts/MainLayout';
import { Routes } from '@src/Routes';
import React from 'react';
import { hot } from 'react-hot-loader';
import { BrowserRouter } from 'react-router-dom';

export const ClientRouter = hot(module)(() => {
  return (
    <BrowserRouter>
      <MainLayout>
        <Routes />
      </MainLayout>
    </BrowserRouter>
  );
});
