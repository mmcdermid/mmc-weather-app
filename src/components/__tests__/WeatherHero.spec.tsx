import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { WeatherHero } from '../WeatherHero';

describe('<WeatherHero />', () => {
  let wrapper: ShallowWrapper = null;

  test(`Should render a form`, () => {
    wrapper = shallow(<WeatherHero/>);

    expect(wrapper.find('form')).toHaveLength(1);
  });
});
