import { WeatherDay } from '@src/types/Weather';
import React from 'react';

interface Props {
  data: WeatherDay;
}

export const WeatherCard = (props: Props) => (
  <div className="Card">
    <div className="Card__title">{props.data.name}</div>
    {props.data.details.map((detail, i) => (
      <div key={i}>
        <span className="Card__details u-text-muted">{detail.hours}:00</span>
        <span className="Card__details">{detail.description}</span>
        <strong className="Card__details">{Math.round(detail.temperature)}℃</strong>
      </div>
    ))}
  </div>
);
