import { WeatherCard } from '@src/components/WeatherCard';
import { WeatherDay } from '@src/types/Weather';
import React from 'react';
import Col from 'reactstrap/lib/Col';
import Row from 'reactstrap/lib/Row';

interface Props {
  data: WeatherDay[];
}

export const WeatherCardList = (props: Props) => (
  <Row>
    {
      props.data.map((d, i) =>
        <Col xs={6} md={4} key={i}>
          <WeatherCard data={d}/>
        </Col>
      )
    }
  </Row>
);
