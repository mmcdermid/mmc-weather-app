import { WeatherCardList } from '@src/components/WeatherCardList';
import { getFriendlyErrorMessage, weatherApi } from '@src/services/api';
import { APIWeatherData, WeatherDay } from '@src/types/Weather';
import { transformWeatherData } from '@src/utils/weatherTransform';
import React, { ChangeEventHandler, Component, FormEventHandler, MouseEventHandler } from 'react';

interface State {
  data: WeatherDay[];
  error: string;
  city: string;
}

export class WeatherHero extends Component<{}, State> {
  public state: State = {
    error: '',
    city: '',
    data: null
  };

  public getWeather = async () => {
    if (!this.state.city) {
      this.setState({ error: 'Please enter a city' });

      return;
    }

    const response =
      await weatherApi.get<APIWeatherData>('forecast', {
        q: this.state.city,
        APPID: '78c6f2d1fdca238a085ac16a7659437d',
        units: 'metric'
      });

    if (response.ok) {
      const data = transformWeatherData(response.data);
      this.setState({ data, error: '' });
    } else {
      this.setState({ error: getFriendlyErrorMessage(response.problem) });
    }
  };

  private handleReset: MouseEventHandler<HTMLButtonElement> = () => {
    this.setState({ city: '', error: '', data: null });
  };

  private handleSubmit: FormEventHandler<HTMLFormElement> = async e => {
    e.preventDefault();
    this.getWeather();
  };

  private handleChange: ChangeEventHandler<HTMLInputElement> = e => {
    this.setState({ city: e.target.value });
  };

  public render() {
    return (
      <section className="Hero">
        <div className="Hero__content">
          {!this.state.data &&
          <form onSubmit={this.handleSubmit}>
            <div className="Hero__heading h1">
              How's the weather in{' '}
              <div className="Hero__input">
                <label htmlFor="weather-city" className="a11y">City Name</label>
                <input id="weather-city" type="text" className="TextInput" onChange={this.handleChange}/>
              </div>
              <span>?</span>
            </div>

            <button className="Button Button--primary">Search</button>
          </form>}

          {this.state.data &&
          <button className="Hero__close Button Button--primary" onClick={this.handleReset}>Another?</button>
          }

          {this.state.error && <div className="Alert">
            {this.state.error}
          </div>}

          {this.state.data && !!this.state.data.length && <WeatherCardList data={this.state.data}/>}
        </div>
      </section>
    );
  }
}
