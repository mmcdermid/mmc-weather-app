import { ServerConfig } from '@src/server/config';
import { renderMiddleware } from '@src/server/middleware/renderMiddleware';
import express from 'express';
import helmet from 'helmet';
import morgan from 'morgan';

export const startApp = (config: ServerConfig) => {
    const app = express();

    app.use(helmet());
    app.use(morgan('tiny'));

    app.use('/static', express.static('dist/spa'));

    app.set('views', './src/server/views');
    app.set('view engine', 'pug');

    app.use(renderMiddleware);

    app.listen(config.APP_PORT, () => {
      console.log(`Application listening on port ${config.APP_PORT}`); // tslint:disable-line no-console
    });
  }
;
