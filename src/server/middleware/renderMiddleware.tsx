import { ServerRouter } from '@src/server/ServerRouter';
import { Request, Response } from 'express';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import Helmet from 'react-helmet';

export const renderMiddleware = async (req: Request, res: Response) => {
  const context: { [k: string]: any } = {};
  const markup = ReactDOMServer.renderToString((
    <ServerRouter location={req.url} context={context}/>
  ));

  if (context.url) {
    res.writeHead(301, {
      Location: context.url
    });
    res.end();

    return;
  }

  const staticHead = Helmet.renderStatic();

  res.render('index', {
    markup,
    title: staticHead.title.toString(),
    meta: staticHead.meta.toString()
  });
};
