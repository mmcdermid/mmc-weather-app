import 'regenerator-runtime/runtime';
import '../styles/index.scss';

import { startApp } from './app';
import { getConfig } from './config';

const configuration = getConfig(process.env);
startApp(configuration);
