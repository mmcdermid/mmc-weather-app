import { MainLayout } from '@src/layouts/MainLayout';
import { Routes } from '@src/Routes';
import React from 'react';
import { StaticRouter, StaticRouterProps } from 'react-router';

export function ServerRouter(props: Partial<StaticRouterProps>) {
  return (
    <StaticRouter {...props}>
      <MainLayout>
        <Routes/>
      </MainLayout>
    </StaticRouter>
  );
}
