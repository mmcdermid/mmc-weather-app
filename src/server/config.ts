import ProcessEnv = NodeJS.ProcessEnv;

const config = {
  APP_NAME: process.env.APP_NAME || 'mmc-weather-app',
  NODE_ENV: process.env.NODE_ENV || 'development',
  APP_PORT: process.env.APP_PORT || 8080,
};

export type ServerConfig = typeof config;

export function getConfig(env: ProcessEnv) {
  return { ...env, ...config };
}
