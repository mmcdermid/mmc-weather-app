import React from 'react';
import { Link } from 'react-router-dom';
import Col from 'reactstrap/lib/Col';
import Row from 'reactstrap/lib/Row';

export function NotFound() {
  return (
    <section>
      <Row>
        <Col>
          <h1>Uh oh...</h1>
          <h2>Looks like nobody's home.</h2>
          <p>The page you have requested cannot be found. It may have moved or the address typed is incorrect.</p>
          <p><Link to="/">Return to Homepage</Link></p>
        </Col>
      </Row>
    </section>
  );
}
