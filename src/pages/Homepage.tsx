import { WeatherHero } from '@src/components/WeatherHero';
import React from 'react';

export function Homepage() {
  return (
    <>
      <div><h1 className="a11y">Weather App</h1></div>
      <WeatherHero />
    </>
  );
}
