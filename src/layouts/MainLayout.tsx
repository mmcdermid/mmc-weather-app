import React, { Component } from 'react';
import Helmet from 'react-helmet';
import Container from 'reactstrap/lib/Container';

export class MainLayout extends Component {
  public render() {
    return (
      <>
        <Helmet>
          <title>Homepage | Weather App</title>
        </Helmet>

        <main>
          <Container>
            {this.props.children}
          </Container>
        </main>
      </>
    );
  }
}
