import { APIWeatherData, WeatherDay } from '@src/types/Weather';

export const transformWeatherData = (data: APIWeatherData): WeatherDay[] => {
  return data.list.reduce(
    (days: WeatherDay[], apiItem): WeatherDay[] => {
      const date = new Date(apiItem.dt * 1000);

      const dayName = date.toLocaleString('en-gb', { weekday: 'long' });
      const hours = date.getHours();

      let day = days.find(d => d.name === dayName);

      if (!day) {
        day = {
          name: dayName,
          details: []
        };
        days.push(day);
      }

      day.details.push({
        description: apiItem.weather.length ? apiItem.weather[0].main : '',
        temperature: apiItem.main.temp,
        hours
      });

      return days;
    },
    []
  );
};
