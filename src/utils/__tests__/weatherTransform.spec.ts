import { APIWeatherData } from '../../types/Weather';
import { transformWeatherData } from '../weatherTransform';

describe('Utilities: weatherTransform', () => {
  const fakeData: APIWeatherData = {
    list: [
      { weather: [{ main: 'Clear' }], dt: new Date(2018, 8, 17, 12).getTime() / 1000, main: { temp: 21.4 }}, // Monday
      { weather: [{ main: 'Cloud' }], dt: new Date(2018, 8, 17, 12).getTime() / 1000, main: { temp: 6 }}, // Monday
      { weather: [{ main: 'Rainy' }], dt: new Date(2018, 8, 18, 12).getTime() / 1000, main: { temp: 6.8 }}, // Tuesday
      { weather: [{ main: 'Rainy' }], dt: new Date(2018, 8, 19, 12).getTime() / 1000, main: { temp: 12 }} // Thursday
    ]
  };

  it(`Should create an array, sorted by day`, () => {
    const transformed = transformWeatherData(fakeData);

    expect(transformed[0].name).toEqual('Monday');
    expect(transformed[1].name).toEqual('Tuesday');
    expect(transformed.length).toEqual(3);
  });

  it(`Should add 2 weather items from the same day to the same item`, () => {
    const transformed = transformWeatherData(fakeData);
    expect(transformed[0].details.length).toEqual(2);
  });
});
