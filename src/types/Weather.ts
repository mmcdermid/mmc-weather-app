export interface APIWeatherDataItem {
  dt: number;
  main: {
    temp: number;
  };
  weather: {
    main: string;
  }[];
}

export interface APIWeatherData {
  list: APIWeatherDataItem[];
}

export interface WeatherDay {
  name: string;
  details: WeatherDetails[];
}

export interface WeatherDetails {
  hours: number;
  temperature: number;
  description: string;
}
