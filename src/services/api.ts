import apisauce, { PROBLEM_CODE } from 'apisauce';

export const weatherApi = apisauce.create({
  baseURL: 'http://api.openweathermap.org/data/2.5/',
  timeout: 10000
});

export const getFriendlyErrorMessage = (errorCode: PROBLEM_CODE): string => {
  switch (errorCode) {
    case 'CLIENT_ERROR':
      return 'Please try another city';
    case 'CONNECTION_ERROR':
      return `Sorry, we can't get a hold of the server right now.`;
    case 'NETWORK_ERROR':
      return 'Please try again with a working internet connection.';
    case 'TIMEOUT_ERROR':
      return `Our server didn't respond in time, please try again`;
    default:
    case 'SERVER_ERROR':
      return `We're having some issues, please try again later.`;
  }
};
