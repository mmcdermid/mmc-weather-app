import { Homepage } from '@src/pages/Homepage';
import { NotFound } from '@src/pages/NotFound';
import React from 'react';
import { Route, Switch } from 'react-router-dom';

export const Routes = () => (
  <Switch>
    <Route exact path="/" component={Homepage}/>
    <Route component={NotFound}/>
  </Switch>
);
