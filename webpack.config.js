const merge = require('webpack-merge');
const client = require('./webpack.client');
const server = require('./webpack.server');
const path = require('path');

const shared = {
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: ['babel-loader', {
          loader: 'awesome-typescript-loader',
        }],
      },
    ],
  },
  resolve: {
    alias: {
      '@src': path.resolve(__dirname, 'src/'),
      '@test': path.resolve(__dirname, 'test/')
    },
    extensions: ['.ts', '.tsx', '.js', '.jsx'],
  }
};

module.exports = process.env.NODE_SSR ? merge(shared, server) : merge(shared, client);
