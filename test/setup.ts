import 'regenerator-runtime/runtime'; // tslint:disable-line
import Enzyme from 'enzyme'; // tslint:disable-line
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new(<any> Adapter)() });
