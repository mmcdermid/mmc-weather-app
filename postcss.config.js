module.exports = {
  plugins: {
    'postcss-flexbugs-fixes': {},
    autoprefixer: {
      browsers: [
        'ios >= 9',
        'ie >= 11',
        'last 2 versions'
      ]
    },
    cssnano: process.env.NODE_ENV === 'production' ? {} : null
  }
};
