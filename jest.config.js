module.exports = {
  globals: {
    'ts-jest': {
      useBabelrc: true
    }
  },
  moduleFileExtensions: [
    'ts',
    'tsx',
    'js'
  ],
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest'
  },
  setupFiles: [
    '<rootDir>/test/setup.ts'
  ],
  testMatch: [
    '**/*/*.spec.(ts|tsx|js)'
  ],
  moduleNameMapper: {
    '^@src(.*)$': '<rootDir>/src/$1',
    '^@test(.*)$': '<rootDir>/test/$1',
  }
};
