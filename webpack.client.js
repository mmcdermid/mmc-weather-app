const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const webpack = require('webpack');

const isProd = process.env.NODE_ENV === 'production';

const publicPath = '/';

const plugins = [
  !isProd && new HtmlWebpackPlugin(),
  isProd && new StyleLintPlugin({ syntax: 'scss' }),
  new webpack.optimize.AggressiveMergingPlugin(),
  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    minChunks: Infinity
  }),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
    }
  }),
  !isProd && new webpack.NamedModulesPlugin(),
  isProd && new webpack.optimize.UglifyJsPlugin(),
];

module.exports = {
  entry: {
    bundle: './src/client/client',
    vendor: [
      'babel-polyfill',
      'react',
      'react-dom',
    ]
  },
  output: {
    path: path.join(__dirname, 'dist/spa'),
    filename: '[name].js',
    publicPath
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: isProd ? 'ignore-loader' :
          [
            'style-loader',
            'css-loader?importLoaders=1',
            'postcss-loader',
            'sass-loader'
          ]
        ,
      }
    ]
  },
  devServer: {
    port: process.env.APP_PORT || 1337,
    host: '0.0.0.0',
    noInfo: true,
    historyApiFallback: true,
    publicPath: publicPath,
    stats: {
      assets: false,
      colors: true,
      version: false,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false
    },
    proxy: {
      '/api/*': {
        target:  process.env.HTTP_PROXY_ADDRESS || 'http://localhost:3000/api/',
        secure: false,
        changeOrigin: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    },
  },
  plugins: plugins.filter(Boolean)
};
