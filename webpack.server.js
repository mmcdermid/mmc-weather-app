const path = require('path');
const nodeExternals = require('webpack-node-externals');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: ['./src/server/server'],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.server.js'
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            'css-loader?importLoaders=1',
            'postcss-loader',
            'sass-loader',
          ]
        })
      }
    ],
  },
  target: 'node',
  externals:  [nodeExternals({
    // this WILL include `@src` and `@test` in the bundle
    whitelist: [/^@src/, /^@test/]
  })],
  plugins: [
    new ExtractTextPlugin('spa/bundle.css'),
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'production'
    })
  ]
};
