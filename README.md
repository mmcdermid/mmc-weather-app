# Weather APP
http://mmc-weather-app.uksouth.cloudapp.azure.com/

## Running the app
Requires node (9.9+)

### Dev (HR webpack-dev-server)
```bash
$ npm start
```

### Production (Express server)
```bash
$ npm run build && npm run server
```

## Running tests
```bash
$ npm run build && npm run server
```

## Tech Debt
- Isn't Responsive
- Needs tests for all the components (And component methods being mocked)
- Would generally use Redux for async calls in a real app.
- Doesn't quite lint in time!
- Refactoring to smaller components
- Remove bootstrap grid
- Loading spinner / disable submit on load
- HTTPS
- Almost certainly doesn't work in IE.
